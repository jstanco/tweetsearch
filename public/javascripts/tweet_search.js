// Load the application once the DOM is ready, using `jQuery.ready`:
$(function(){

  Backbone.pubSub = _.extend({}, Backbone.Events);

  var Tweet = Backbone.Model.extend({

    // Default attributes for a tweet
    defaults: function() {
      return {
        from_user: "Twitter User",
        from_user_name: "Twitter User",
        profile_image_url: "https://si0.twimg.com/sticky/default_profile_images/default_profile_3_normal.png",
        id: null,
        text: null,
        source: null,
        created_at: null,
        favorite: false
      };
    }

  });

  // List for Twitter search results
  var TweetList = Backbone.Collection.extend({
    model: Tweet,
    localStorage: new Backbone.LocalStorage("tweets-backbone")
  });

  // Separate list for storing favorite tweets
  var FavoriteList = Backbone.Collection.extend({
    model: Tweet,
    localStorage: new Backbone.LocalStorage("favorites-backbone")
  });

  var Tweets = new TweetList, 
    Favorites = new FavoriteList;

  // The DOM element for a tweet list item
  var TweetView = Backbone.View.extend({

    tagName:  "div",

    // Template for tweet markup
    tweetTemplate: window.JST['tweet_template'],

    events: {
      "click. .js-toggle-fav": "toggleFavorite"
    },

    initialize: function() {
      this.model.on('change', this.render, this);
      this.model.on('destroy', this.remove, this);
    },

    // Populate tweet template with data from model and set default classes
    render: function() {

      this.$el.html(this.tweetTemplate(this.model.toJSON()))
        .addClass("js-stream-item stream-item stream-item expanding-stream-item");

      // Add favorited class if the tweet is a favorite
      if (this.model.get("favorite")) {
        this.$el.children().first().addClass("favorited");
      }
      return this;
    },

    // Add or remove tweet from favorite tweets list
    toggleFavorite: function() {
      var model = this.model;
      model.set("favorite", !model.get("favorite"));

      if (model.get("favorite")) {

        // Skip if the tweet has already been favorited
        if (Favorites.find(function(fav) { return fav.get("id") === model.get("id") })) return;

        // Create a favorite tweet from the selected tweet
        Favorites.create({
          from_user: model.get("from_user"),
          from_user_name: model.get("from_user"),
          profile_image_url: model.get("profile_image_url"),
          id: model.get("id"),
          text: model.get("text"),
          source: model.get("source"),
          created_at: model.get("created_at"),
          favorite: model.get("favorite")
        });

      } else {

        // Remove the tweet from favorites list
        Favorites.forEach(function(fav) {
          if (fav.get("id") === model.get("id")) {
            fav.destroy();
          }
        });

      }
    }

  });


  // The Application
  // ---------------

  var AppView = Backbone.View.extend({

    el: $("#tweetsearch"),
    resultsEl: $("#twitter-search-results"),
    favEl: $("#favorite-tweets"),

    // For switching to favorites view
    favoritesLink: $("#favorites-link"),
    favoritesLinkTemplate: window.JST['favorites_link_template'],

    // For tracking pagination of Twitter search results
    currPage: 1,

    events: {
      "keypress #query":  "handleKeyPress",
      "click #clear-search": "clearSearch",
      "click #favorites-link": "clearSearch",
      "click #clear-favorites": "clearFavorites",
      "click #previous-page": "previousPage",
      "click #next-page": "nextPage"
    },
    errored: false,

    initialize: function() {

      Backbone.pubSub.on('onZeroResults', this.zeroResults, this);
      Backbone.pubSub.on('onError', this.onError, this);
      Backbone.pubSub.on('onClear', this.clear, this);
      Backbone.pubSub.on('onSearchDone', this.searchDone, this);

      this.input = this.$("#query");

      Tweets.on('add', this.addTweet, this);

      Favorites.on('add', this.addFavorite, this);
      Favorites.on('add', this.render, this);
      Favorites.on('remove', this.render, this);

      // Show favorite tweets when first loading
      Favorites.on('reset', this.populateFavorites, this);

      this.footer = this.$('footer');
      this.main = $('#main');

      // Load favorites from local storage
      Favorites.fetch();
    },

    render: function() {

      // Update favorites count
      this.favoritesLink.html(this.favoritesLinkTemplate({count: Favorites.length}));

      if (Favorites.length || Tweets.length) {
        // If we have Twitter search results, then hide favorite tweets view
        if (Tweets.length) {
          this.$el.addClass("search-mode");
        } else {
          this.$el.removeClass("search-mode");
        }
        this.footer.removeClass("hidden");
      } else {
        this.footer.addClass("hidden");
      }
      $("body").removeClass("loading").addClass("loaded");
    },

    // Add a tweet to Twitter search results
    addTweet: function(tweet) {
      var view = new TweetView({model: tweet});
      this.resultsEl.append(view.render().el);
    },

    // Add a tweet to favorite tweet list
    addFavorite: function(tweet) {
      var view = new TweetView({model: tweet});
      this.favEl.append(view.render().el);
    },

    // Populate list of favorite tweets
    populateFavorites: function() {
      var view = this;
      Favorites.forEach(function(fav) {
        view.addFavorite(fav);
      });
      this.render();
    },

    // Decrement current pagination index and execute query with new page
    previousPage: function() {
      this.currPage--;
      this.executeQuery();
    },

    // Increment current pagination index and execute query with new page
    nextPage: function() {
      this.currPage++;
      this.executeQuery();
    },

    // Execute query when carriage return is detected and reset pagination index
    handleKeyPress: function(e) {
      if (e.keyCode != 13) return;
      this.currPage = 1;
      this.executeQuery();
    },

    // Change visiblity of pagination links based on results
    togglePaginationLinks: function(nextPage) {
      var pageEl = $("#pagination"),
        prevEl = $("#previous-page"),
        nextEl = $("#next-page");
      if (nextPage) {
        if (this.currPage == 1) {
          prevEl.addClass("inactive");
        } else {
          prevEl.removeClass("inactive");
        }
        nextEl.removeClass("inactive");
        pageEl.removeClass("inactive");
      } else {
        if (this.currPage > 1) {
          nextEl.addClass("inactive");
        } else {
          pageEl.addClass("inactive");
        }
      }
    },

    // Query the Twitter API via Rails endpoint
    executeQuery: function(e) {

      var term, dateRegex, regextResult, favoriteResult,
        view = this;

      if (!view.input.val()) return;

      // Clear search results, but not text field
      view.clearSearch(false);
      this.searching = true;
      $("body").addClass("searching");
      term = this.input.val();
      dateRegex = /^(?:\w*),\s+(\d+\s+\w+)(?:\s+\d{4}.*)$/;

      $.ajax("/twitter/query/" + term + "/" + view.currPage, {
        type: "GET",
        dataType: 'json',
        success: function(data) {
          view.errored = false;
          Backbone.pubSub.trigger("onSearchDone");
          if (data.results.length) {
            view.togglePaginationLinks(data.next_page);
            data.results.forEach(function(v) { 

              favoriteResult = Favorites.find(function(fav) { return fav.get("id") === v.id });
              regexResult = dateRegex.exec(v.created_at);

              Tweets.create({
                from_user: v.from_user,
                from_user_name: v.from_user_name,
                profile_image_url: v.profile_image_url,
                id: v.id_str,
                text: v.text,
                source: v.source,
                created_at: regexResult.length > 1 ? regexResult[1] : v.created_at,
                favorite: favoriteResult
              });
            });
          } else {
            Backbone.pubSub.trigger("onSearchDone").trigger('onZeroResults');          
          }
          view.render();

        },
        error: function(e) {
          $("body").removeClass("searching");
          Backbone.pubSub.trigger("onError");
        }
      });

    },

    // Clear the search results
    clearSearch: function(clearInput) {

      Backbone.pubSub.trigger("onClear");

      _.chain(Tweets.models).clone().each(function(model){
        model.destroy();
      });

      // Reset navigation to inactive
      $("#pagination").addClass("inactive");

      // Clear input unless we are entering a new search term
      if (clearInput || typeof clearInput == "undefined") {
        this.input.val('');
        this.render();
      }        
      return false;
    },

    // Clear favorites list
    clearFavorites: function() {
      _.chain(Favorites.models).clone().each(function(model){
        model.destroy();
      });
      this.render();
      return false;
    },

    onError: function() {
      this.errored = true;
      this.$el.addClass("errored");
      this.resultsEl.append("Error connecting to Twitter. Try again later.");
    },
    zeroResults: function() {
      this.errored = true;
      this.$el.addClass("zero-results");
      this.resultsEl.append("No results, keep trying.");
    },
    clear: function() {
      this.errored = false;
      this.$el.removeClass("errored zero-results");
      this.resultsEl.empty();      
    },
    searchDone: function() {
      this.searching = true;
      $("body").removeClass("searching");
    }

  });

  // Start the backbone app
  var App = new AppView;

});

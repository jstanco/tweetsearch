class TwitterController < ApplicationController

  require 'net/http'
  require 'json'

  def query 

    term = CGI::escape(params[:term])
    page = params[:page].to_i || 1
    url = URI.parse("http://search.twitter.com/search.json")
    req = Net::HTTP::Get.new("#{url.path}?q=#{term}&page=#{page}")
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    response = res.body

    respond_to do |format|
      format.json { render :json => response }
    end    

  end
end
